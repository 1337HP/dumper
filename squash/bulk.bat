FOR /F "tokens=* USEBACKQ" %%F IN (`call epoch.bat`) DO SET nam=%%F
echo %date% @ %time% >> %nam%-bulk.log

set target_dir=C:\\Users\\imkoolurnot\\Pictures\\Screenshots\\
set "ts=%~2"

for %%a in (*.png) do set filename=%%a & call :Loopbody
goto :EOF

:Loopbody
if not defined ts for /f "skip=1 delims=" %%i in ('WMIC DATAFILE WHERE "NAME='%target_dir%%filename%'" GET CreationDate') do if not defined ts set ts=%%i
FOR /F "tokens=* USEBACKQ" %%F IN (`call timeaux.bat`) DO SET ver=%%F
magick "%filename%" -set comment "%ts%" %ver%.jpg
::magick "%filename%" -sampling-factor 4:2:0 %ver%420.jpg

echo %ver% of %ts% >> %nam%-bulk.log

set "ts=%~2"

:EOF