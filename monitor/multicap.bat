FOR /F "tokens=* USEBACKQ" %%F IN (`call epochex.bat`) DO SET ver=%%F
ffmpeg.exe -f dshow -i video="screen-capture-recorder":audio="virtual-audio-capturer"^
 -preset ultrafast -crf 18 "%ver%_local.mkv"^
 "%ver%_s1.wav"
pause