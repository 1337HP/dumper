set filename=%1
for %%a in (*.vob) do set filename=%%a & call :Loopbody
goto :eof
:Loopbody	
for /f "tokens=14" %%i in ('ffmpeg.exe -y -i "%filename%" -t 10 -vf cropdetect -f null - 2^>^&1') do set cropvalue=%%i
ffplay.exe -i "%filename%" -vf "bwdif,%cropvalue%
set filename=%1
goto :eof