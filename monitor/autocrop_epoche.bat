set filename=%1
FOR /F "tokens=* USEBACKQ" %%F IN (`call epochex.bat`) DO SET ver=%%F
echo %ver%
for /f "tokens=14" %%i in ('ffmpeg.exe -i "%filename%.mp4" -t 60 -vf cropdetect -f null - 2^>^&1') do set cropvalue=%%i
::ffmpeg.exe -i "%filename%.vob" -vf %cropvalue%,bwdif,scale=-16:720,pad="ih*16/9/sar:ih:(ow-iw)/2:(oh-ih)/2" "%ver%.mkv"
ffmpeg.exe -i "%filename%.mp4" -vf %cropvalue%,scale=-16:720 -preset veryfast -acodec copy "%ver%.mkv"