set filename=%1
for %%a in (*.ts) do set filename=%%a & call :Loopbody
goto :EOF

:Loopbody
for /f "tokens=*" %%i in ('ffprobe.exe -v "error" -of "flat=s=_" -select_streams "v:0" -show_entries "stream=sample_aspect_ratio" "%filename%"') do set SAR=%%i
set str0=%SAR::= %
set str0=%str0:"= %
for /f "tokens=2 usebackq" %%a in ('%str0%') do set widsar=%%a
for /f "tokens=3 usebackq" %%a in ('%str0%') do set heisar=%%a
for /f "tokens=*" %%i in ('ffprobe.exe -v error -of "flat=s=_" -select_streams v:0 -show_entries "stream=width" "%filename%"') do set width=%%i
for /f "tokens=2 usebackq" %%a in ('%width%') do set width=%%a
for /f "tokens=*" %%i in ('ffprobe.exe -v error -of "flat=s=_" -select_streams v:0 -show_entries "stream=height" "%filename%"') do set height=%%i
for /f "tokens=2 usebackq" %%a in ('%height%') do set height=%%a
for /f "tokens=14" %%i in ('ffmpeg.exe -i "%filename%" -t 10 -vf cropdetect -f null - 2^>^&1') do set cropvalue=%%i
set str1=%cropvalue::= %
for /f "tokens=2 usebackq" %%a in ('%str1%') do set widcrop=%%a
for /f "tokens=3 usebackq" %%a in ('%str1%') do set heicrop=%%a
set /a aux1=(%widcrop%*%widsar%*1000)/(%heicrop%*%heisar%)
echo %aux1%
set /a aux2=1778
set /a finalwid=(((720*%aux1%)/1000)/16)*16
set /a finalhei=((1280000/%aux1%)/16)*16
echo %finalhei% x %finalwid%

if %aux1% GEQ %aux2% (
	call :Widescale
) ELSE (
	call :Squarescale
)
goto :eof

:Squarescale
ffmpeg.exe -y -i "%filename%" -vf "yadif,%cropvalue%,scale=-16:%height%" -acodec copy "%filename%".mkv
set filename=%1
goto :EOF

:Widescale
ffmpeg.exe -y -i "%filename%" -vf "yadif,%cropvalue%,scale=%width%:%finalhei%" -acodec copy "%filename%".mkv
set filename=%1
goto :EOF
 
